\beamer@endinputifotherversion {3.33pt}
\beamer@sectionintoc {1}{Motivation}{3}{0}{1}
\beamer@sectionintoc {2}{System Proposal}{8}{0}{2}
\beamer@subsectionintoc {2}{1}{Energy Harvesting}{8}{0}{2}
\beamer@subsectionintoc {2}{2}{Radio and Microcontroller}{10}{0}{2}
\beamer@subsectionintoc {2}{3}{Light Sensor Geolocation}{12}{0}{2}
\beamer@subsectionintoc {2}{4}{GPS based Geolocation}{15}{0}{2}
\beamer@sectionintoc {3}{Comparison}{19}{0}{3}
\beamer@subsectionintoc {3}{1}{Power Budget}{19}{0}{3}
\beamer@subsectionintoc {3}{2}{Weight Budget}{21}{0}{3}
\beamer@sectionintoc {4}{Conclusion}{22}{0}{4}
