* Bird monitoring system for monitoring of the breeding behavior as well as the migration of the sandwich tern
** Project Scope
*** Motivation
The scope of this report is an energy autonomous monitoring device for the migration of sea birds, based on the example of the sandwich tern. 
**** Technical
 - traditional method of rings provides only a verry sparse set of datapoints for migration
 - light level geolocators have been developed and used by engineers of the British Antarctic Survey
 - newer GPS based devices also available
 - no energy autonomous design known to the authors yet
**** Scientific
The exact migration route of the Sandwich Tern is not known, however ring finds in the mediteranian sea [6]
** Specifications
*** Sensor Location
 - as a ring on the foot of the animal 
*** Weight 
 - under 2% of the body weight, prefereably below 1% [1] page 8
 - weight of 210g to 260g [2]
 - as such: total sensor weight below 4.2g, ideally 2.1g

*** Aquesition requirements
 - position data over a longer period of time (at least one year), at least one data point per day
 - possibly temperature
 - possibly wetness indication (to monitor hunting activity)

*** Energy harvesting
 - State of the Art: primary cell battery powered devices
**** Constraints
 - very limited size and weight 
 - outdoor environment
 - seawater resistence
 - rather high energy requirement
**** Energy conversion
 - thermoelectric: no exploitable gradient, possibly heavy
 - mechanic: weight limit reduces the possible gain
 - Solar power most promising
  - Cell placed below the animal
   - Indirect ilumination
**** Energy storage
 - LiPo batterys
  - high energy density
  - hard to find in small size/weight, around 50mAh reasonable
  - still quite large
 - Li-Mn
  - small coin cells

*** Processing
 - MSP430fr2433 
   - RTC enabled sleep at 0.9 t 3 uA
   - 16 Bit 
   + Capacitive touch
   + 128 KB FRAM
   + 4x4 mm VQFN package
 - Energy Micro Zero Gecko
   + 32 Bit Cortex M0
   - 0.9 uA RTC enabled deep sleep mode
   - 4 to 32 KB Flash 
   + Capacitive Touch
   - 5x5mm QFN package
 - Energy Micro Sleepy Bee
   - 8 Bit 8051 derivate
   + 8 KB flash
   + 0.5 uA RTC enabled sleep
   + 3x3mm QFN package
 
** Sensing Case Studies
*** Light based geolocation
**** Idea
Light based geolocators use the actual time of sunrise and sunset to estimate a longitudinal coordinate as well as the absolute day length for an estimation of a latitudal coordinate. This kind of devices have been pioneered by engineers of the British Arctic Survey and are now available comercially [3]. The achieveable accuracy is specified around +/-150 km [3], the deviation is mainly caused by shading effects due to cloud overcast. However this accuracy is good enough to obtain a rough idea of the migration, considering that terns, in this case based on a study of the equaly sized common tern, migrate with daily legs of up to 500km [4].

**** System Proposal
With the availability of cheap and very low powered color sensors, conveniently attachable over I2C the state of the art method could be improved by replacing the sensing of light levels which are disturbed by overcasting and shadowing with the sensing of shifts in the spectral distribution to determine the color shifts of natural light during dawn and dusk. Using capacitive touch sensing wetness can be detected and as souch it is possible to log resting on the water surface as well as plunge dives while hunting. If the bird rests on the water surface during light measurements the capacitive touch sensing can also be used to compensate for the light attenuation due to the water around the submerged sensor. 

**** Power Budget
**** Weight Budget
**** Data Budget
**** Discussion and Conclusion
*** GPS based geolocation
**** Idea
**** System Proposal
**** Power Budget
**** Weight Budget
**** Data Budget
**** Discussion and Conclusion

** Data Readout
*** Wireless Data Readout
**** Backscattering
**** Active transmission
*** Recapture
Not to hard at the nesting site [5]

** Integration
 - Sea water sealing could be achieved best with a complete podding of the device in a clear sea water proof epoxy
 - Flex PCBs allow for a very light weight 3D integration of the whole circuitry to reduce overall weight and size
 - Wireless readout together with solar powering removes the need for any electrical contacts, reports show that these are the main cause of failure. Water sensing can still be done using the capacitive touch interfaces on the UC.
 - The whole design has to be connected to the leg of the animal, apropriate fastining methods have to be designed into the housing

** Conclusion

** References
[1] http://www.arctictern.info/carsten/pdf/Geolocator_manual_v7.pdf
[2] http://www.vogelwarte.ch/en/birds/birds-of-switzerland/sandwich-tern.html
[3] http://www.birdtracker.co.uk/
[4] http://www.arctictern.info/carsten/metoder.html
[5] https://www.youtube.com/watch?v=ukqOFJYH0xA
[6] F. Bairlein et al., Atlas des Vogelzugs [Aula Verlag, 2014], ISBN-978-3-89104-770-5, page 297ff
[7] Tracking of Arctic terns Sterna paradisaea reveals longest animal migration, C. Egevang et al. 
